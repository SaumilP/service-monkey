package za.co.discovery.apps.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import za.co.discovery.apps.domain.WindowsService
import za.co.discovery.apps.service.ServiceControlService

import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Component
@Path("/api/v1/services")
class ServiceControl {

    @Autowired
    private ServiceControlService serviceControlService

    @GET
    //@Consumes({ MediaType.APPLICATION_JSON })
    //@Produces({ MediaType.APPLICATION_JSON })
    public WindowsService listServices(String serverName) {
        return new WindowsService()
    }
}
