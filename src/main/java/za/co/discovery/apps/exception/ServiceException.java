package za.co.discovery.apps.exception;

public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = -4108562662648344829L;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
