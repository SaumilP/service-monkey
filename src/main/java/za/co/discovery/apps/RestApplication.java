package za.co.discovery.apps;

import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/api/rest")
public class RestApplication extends Application {
}
