package za.co.discovery.apps.service.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import za.co.discovery.apps.domain.ProcessStatus;
import za.co.discovery.apps.domain.ServiceAction;
import za.co.discovery.apps.domain.WindowsService;
import za.co.discovery.apps.service.ServiceControlService;
import za.co.discovery.apps.service.WindowsServiceListService;

@Service("ServiceControl")
public class ServiceControlServiceImpl implements ServiceControlService {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceControlServiceImpl.class);


    @Autowired
    @Qualifier("WindowsService")
    private WindowsServiceListService windowsService;

    @Override
    public void start(String serviceName, String hostIPAddress) {
        LOG.debug("Starting Service[{}] on Server[{}]", serviceName, hostIPAddress);

        windowsService.execute(hostIPAddress, serviceName, ServiceAction.START_SERVICE);

        LOG.debug("Service[{}] started on Server[{}]", serviceName, hostIPAddress);
    }

    @Override
    public void stop(String serviceName, String hostIPAddress) {
        LOG.debug("Stopping Service[{}] on Server[{}]", serviceName, hostIPAddress);

        windowsService.execute(hostIPAddress, serviceName, ServiceAction.STOP_SERVICE);

        LOG.debug("Service[{}] stopped on Server[{}]", serviceName, hostIPAddress);
    }

    @Override
    public ProcessStatus getServiceStatus(String serverName, String serviceName) {
        LOG.debug("Retrieving Service[{}] Status from Server[{}]", serviceName, serverName);

        WindowsService winService = this.windowsService.get(serverName, serviceName);

        return winService.getCurrentStatus();
    }
}
