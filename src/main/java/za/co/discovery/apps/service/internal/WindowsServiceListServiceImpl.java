package za.co.discovery.apps.service.internal;

import org.jinterop.dcom.core.IJIComObject;
import org.jinterop.dcom.core.JIArray;
import org.jinterop.dcom.core.JIString;
import org.jinterop.dcom.core.JIVariant;
import org.jinterop.dcom.impls.JIObjectFactory;
import org.jinterop.dcom.impls.automation.IJIDispatch;
import org.jinterop.dcom.impls.automation.IJIEnumVariant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.discovery.apps.domain.ServiceAction;
import za.co.discovery.apps.domain.WindowsService;
import za.co.discovery.apps.exception.ServiceException;
import za.co.discovery.apps.service.WindowsServiceListService;

import javax.annotation.Nonnull;
import java.util.List;

import static za.co.discovery.apps.domain.Constants.CONNECT_SERVER;
import static za.co.discovery.apps.domain.Constants.WIN32_NAMESPACE;
import static za.co.discovery.apps.domain.Constants.WIN32_SERVICE_QRY;

/**
 * Service responsible for providing methods for controlling windows services
 *
 * {@link https://www.1e.com/blogs/2009/09/23/wmic-aliaswmi-class-mapping/} for more information on underlying queries using WMI
 */
@Service("WindowsService")
public class WindowsServiceListServiceImpl implements WindowsServiceListService {

    private static final Logger LOG = LoggerFactory.getLogger(WindowsServiceListServiceImpl.class);

    @Autowired
    private IJIDispatch wcomLocator;

    @Override
    public List<WindowsService> list() {
        return null;
    }

    @Override
    public WindowsService get(@Nonnull String hostIPAddress, @Nonnull String serviceName) {
        LOG.debug("Retrieving Service[{}] information from Server[{}]", serviceName, hostIPAddress);

        return null;
    }

    public void execute(@Nonnull String hostIPAddress, @Nonnull String serviceName, ServiceAction action) {
        try {
            IJIDispatch wbemServices = createCOMServer(hostIPAddress);

            /**
             * Flags for the query
             */
            final int RETURN_IMMEDIATE = 0x10;  //16
            final int FORWARD_ONLY = 0x20;      //32

            Object[] params = new Object[]{
                    new JIString(String.format(WIN32_SERVICE_QRY, serviceName)),
                    JIVariant.OPTIONAL_PARAM(),
                    new JIVariant(new Integer(RETURN_IMMEDIATE + FORWARD_ONLY))
            };

            /*
             * Execute the query
             */
            JIVariant[] servicesSet = wbemServices.callMethodA("ExecQuery", params);
            IJIDispatch wbemObjectSet = (IJIDispatch) JIObjectFactory.narrowObject(servicesSet[0].getObjectAsComObject());

            IJIComObject enumComObject = wbemObjectSet.get("_NewEnum").getObjectAsComObject();
            IJIEnumVariant enumVariant = (IJIEnumVariant) JIObjectFactory.narrowObject(enumComObject.queryInterface(IJIEnumVariant.IID));

            /*
             * Process the result
             */
            Object[] elements = enumVariant.next(1);
            JIArray aJIArray = (JIArray) elements[0];
            JIVariant[] array = (JIVariant[]) aJIArray.getArrayInstance();

            /*
             * Loop through each found services
             */
            for (JIVariant variant : array) {
                IJIComObject procComObj = variant.getObjectAsComObject();
                IJIDispatch wbemObjectDispatch = (IJIDispatch) JIObjectFactory.narrowObject(procComObj);

                // Print object as text.
                JIVariant[] serviceObject = wbemObjectDispatch.callMethodA("GetObjectText_", new Object[]{1});
                LOG.debug("{}", serviceObject[0].getObjectAsString().getString());

                // Start or Stop the service
                String methodToInvoke = action.getActionCd();
                JIVariant returnStatus = wbemObjectDispatch.callMethodA(methodToInvoke);

                // if return code = 0 success. See MSDN for more details about the method.
                LOG.debug("Return status: {}", returnStatus.getObjectAsInt());
            }
        } catch (Exception e) {
            LOG.error("Failed to execute service command", e.getMessage(), e);
        }
    }

    private IJIDispatch createCOMServer(final String hostIPAddress) throws ServiceException {
        try {
            Object[] params = new Object[]{
                    new JIString(hostIPAddress),        //strServer
                    new JIString(WIN32_NAMESPACE),      //strNamespace
                    JIVariant.OPTIONAL_PARAM(),         //strUser
                    JIVariant.OPTIONAL_PARAM(),         //strPassword
                    JIVariant.OPTIONAL_PARAM(),         //strLocale
                    JIVariant.OPTIONAL_PARAM(),         //strAuthority
                    new Integer(0),               //iSecurityFlags
                    JIVariant.OPTIONAL_PARAM()          //objwbemNamedValueSet
            };

            JIVariant results[] = wcomLocator.callMethodA(CONNECT_SERVER, params);

            return (IJIDispatch) JIObjectFactory.narrowObject(results[0].getObjectAsComObject());
        } catch (Exception ex) {
            LOG.error("Failed to create COM Server", ex.getMessage(), ex);
            throw new ServiceException(ex);
        }
    }
}
