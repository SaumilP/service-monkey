package za.co.discovery.apps.service;

import org.springframework.stereotype.Service;

@Service
public interface FileBeatService {

    void start(String serverName);

    void stop(String serverName);

    void restart(String serverName);

    void restartAll();
}
