package za.co.discovery.apps.service;

import org.springframework.stereotype.Service;
import za.co.discovery.apps.domain.ProcessStatus;

@Service
public interface ServiceControlService {
    void start(String hostIPAddress, String serviceName);

    void stop(String hostIPAddress, String serviceName);

    ProcessStatus getServiceStatus(String serverName, String serviceName);
}
