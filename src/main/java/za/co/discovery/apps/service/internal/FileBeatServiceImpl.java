package za.co.discovery.apps.service.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import za.co.discovery.apps.domain.ProcessStatus;
import za.co.discovery.apps.service.FileBeatService;
import za.co.discovery.apps.service.ServiceControlService;

import java.util.List;

@Service("FileBeatService")
public class FileBeatServiceImpl implements FileBeatService {

    private static final Logger LOG = LoggerFactory.getLogger(FileBeatServiceImpl.class);

    @Value("${service.filebeat:filebeat")
    private String serviceName;

    @Value("${servers.filebeat}")
    private List<String> hostIPAddresses;

    @Autowired
    @Qualifier("ServiceControl")
    private ServiceControlService serviceControl;

    @Override
    public void start(String serverName) {
        serviceControl.start(serverName, serviceName);
    }

    @Override
    public void stop(String serverName) {
        serviceControl.stop(serverName, serverName);
    }

    @Override
    public void restart(String serverName) {
        if(isServiceRunning(serverName, serviceName)) {
            serviceControl.stop(serverName, serviceName);
        }
        serviceControl.start(serverName, serviceName);
    }

    private boolean isServiceRunning(String serverName, String serviceName) {
        ProcessStatus status = serviceControl.getServiceStatus(serverName, serviceName);
        return status == ProcessStatus.RUNNING;
    }

    @Override
    public void restartAll() {
        for (String hostIPAddress : hostIPAddresses) {
            restart(hostIPAddress);
        }

        LOG.info("Restarted FileBeat on all the configured servers");
    }
}
