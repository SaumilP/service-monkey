package za.co.discovery.apps.service.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import za.co.discovery.apps.service.ReportCheckService;

@Service("ReportCheckService")
public class ReportCheckServiceImpl implements ReportCheckService {

    private static final Logger LOG = LoggerFactory.getLogger(ReportCheckServiceImpl.class);
}
