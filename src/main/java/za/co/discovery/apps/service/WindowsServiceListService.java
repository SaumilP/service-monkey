package za.co.discovery.apps.service;

import org.springframework.stereotype.Service;
import za.co.discovery.apps.domain.ServiceAction;
import za.co.discovery.apps.domain.WindowsService;

import javax.annotation.Nonnull;
import java.util.List;

@Service
public interface WindowsServiceListService {
    List<WindowsService> list();

    WindowsService get(@Nonnull String hostIPAddress, @Nonnull String serviceName);

    void execute(@Nonnull String hostIPAddress,@Nonnull String serviceName, ServiceAction action);
}
