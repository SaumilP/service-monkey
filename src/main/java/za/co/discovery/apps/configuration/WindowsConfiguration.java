package za.co.discovery.apps.configuration;

import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.common.JISystem;
import org.jinterop.dcom.core.JIComServer;
import org.jinterop.dcom.core.JISession;
import org.jinterop.dcom.impls.automation.IJIDispatch;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import javax.inject.Named;
import java.util.logging.Level;

import static org.jinterop.dcom.core.JIProgId.valueOf;
import static org.jinterop.dcom.impls.JIObjectFactory.narrowObject;
import static org.jinterop.dcom.impls.automation.IJIDispatch.IID;
import static za.co.discovery.apps.domain.Constants.WINDOWS_SERVICE_CONNECTOR;

@Configuration
@ComponentScan(basePackages = "za.co.discovery.apps")
@ImportResource(locations = "application.properties")
public class WindowsConfiguration {


    @Value("${service.domain}")
    private String domainName;

    @Value("${service.nt-user}")
    private String userName;

    @Value("${service.password}")
    private String password;

    @Value("${service.hostIPAddress:127.0.0.1}")
    private String hostIPAddress;

    @Bean
    public JISession dCOMSession() {
        JISystem.getLogger().setLevel(Level.WARNING);
        JISession dCOMSession = JISession.createSession(domainName, userName, password);
        dCOMSession.useSessionSecurity(false);
        JISystem.setAutoRegisteration(false);
        return dCOMSession;
    }

    @Bean
    public IJIDispatch wcomLocator(@Named JISession dCOMSession) throws Exception {
        try {
            JIComServer comServer = new JIComServer(valueOf(WINDOWS_SERVICE_CONNECTOR), hostIPAddress, dCOMSession);

            return (IJIDispatch) narrowObject(comServer.createInstance().queryInterface(IID));
        } finally {
            destroyCOMSession(dCOMSession);
        }
    }

    public void destroyCOMSession(JISession dCOMSession) {
        if (dCOMSession != null) {
            try {
                JISession.destroySession(dCOMSession);
            } catch (JIException e) {
                e.printStackTrace();
            }
        }
    }
}
