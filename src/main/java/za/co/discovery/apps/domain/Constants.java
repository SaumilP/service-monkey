package za.co.discovery.apps.domain;

public final class Constants {

    public static final String WINDOWS_SERVICE_CONNECTOR = "WbemScripting.SWbemLocator";

    public static final String LOCALHOST = "127.0.0.1";

    public static final String WIN32_NAMESPACE = "ROOT\\CIMV2";

    public static final String CONNECT_SERVER = "ConnectServer";

    public static final String WIN32_SERVICE_QRY = "SELECT * FROM Win32_Service WHERE Name = '%s'";
}
