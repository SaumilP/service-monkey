package za.co.discovery.apps.domain;

public class WindowsService {
    private final String serviceName;
    private final ProcessStatus currentStatus;

    public WindowsService(final String serviceName, final ProcessStatus currentStatus) {
        this.serviceName = serviceName;
        this.currentStatus = currentStatus;
    }

    public String getServiceName() {
        return serviceName;
    }

    public ProcessStatus getCurrentStatus() {
        return currentStatus;
    }
}
