package za.co.discovery.apps.domain;

public enum ServiceAction {
    START_SERVICE("StartService"),
    STOP_SERVICE("StopService"),
    NOTHING("None");

    private String actionCd;

    ServiceAction(String actionCd) {
        this.actionCd = actionCd;
    }

    public String getActionCd() {
        return actionCd;
    }

    public ServiceAction parse(final String actionCode) {
        for (ServiceAction action : values()) {
            if (action.getActionCd().equals(actionCode)) {
                return action;
            }
        }
        return ServiceAction.NOTHING;
    }
}
