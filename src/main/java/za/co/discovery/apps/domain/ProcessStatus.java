package za.co.discovery.apps.domain;

public enum ProcessStatus {
    RUNNING,
    STOPPED,
    RESTARTING
}
